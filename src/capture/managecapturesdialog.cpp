/*
    SPDX-FileCopyrightText: 2008 Jean-Baptiste Mardelle <jb@kdenlive.org>

SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "managecapturesdialog.h"
#include "doc/kthumb.h"

#include "kdenlive_debug.h"
#include "klocalizedstring.h"
#include <KFileItem>
#include <QFontDatabase>

#include <QFile>
#include <QIcon>
#include <QPixmap>
#include <QTimer>
#include <QTreeWidgetItem>

ManageCapturesDialog::ManageCapturesDialog(const QList<QUrl> &files, QWidget *parent)
    : QDialog(parent)
{
    setFont(QFontDatabase::systemFont(QFontDatabase::SmallestReadableFont));

    ui.setupUi(this);

    importButton = ui.buttonBox->button(QDialogButtonBox::Ok);
    importButton->setText(i18n("Import"));
    importButton->setEnabled(false);

    ui.treeWidget->setIconSize(QSize(70, 50));

    for (const QUrl &url : files) {
        KFileItem file(url);
        file.setDelayedMimeTypes(true);

        QTreeWidgetItem *item = new QTreeWidgetItem(ui.treeWidget);
        item->setData(0, Qt::UserRole, url.toLocalFile());
        item->setToolTip(0, url.toLocalFile());
        item->setText(0, url.fileName());
        item->setText(1, KIO::convertSize(file.size()));
        item->setCheckState(0, Qt::Checked);
        item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    }

    connect(ui.treeWidget, &QTreeWidget::itemChanged, this, &ManageCapturesDialog::slotRefreshButtons);
    connect(ui.deleteButton, &QAbstractButton::pressed, this, &ManageCapturesDialog::slotDeleteCurrent);
    connect(ui.toggleButton, &QAbstractButton::pressed, this, &ManageCapturesDialog::slotToggle);

    QTreeWidgetItem* item = ui.treeWidget->topLevelItem(0);
    if (item) {
        ui.treeWidget->setCurrentItem(item);
    }

    QTimer::singleShot(500, this, &ManageCapturesDialog::slotCheckItemIcon);

    ui.treeWidget->resizeColumnToContents(0);
    ui.treeWidget->setEnabled(false);

    adjustSize();
}

ManageCapturesDialog::~ManageCapturesDialog() = default;

void ManageCapturesDialog::slotCheckItemIcon()
{
    int itemCount = ui.treeWidget->topLevelItemCount();

    for (int i = 0; i < itemCount; ++i) {
        QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);

        if (item->icon(0).isNull()) {
            QString imageUrl = item->data(0, Qt::UserRole).toString();
            QPixmap pixmap = KThumb::getImage(QUrl(imageUrl), 0, 70, 50);
            item->setIcon(0, QIcon(pixmap));
            ui.treeWidget->resizeColumnToContents(0);
            repaint();
        }
    }

    ui.treeWidget->setEnabled(true);
}


void ManageCapturesDialog::slotRefreshButtons() 
{
    bool itemsChecked = checkForCheckedItems();
    importButton->setEnabled(itemsChecked);
}

bool ManageCapturesDialog::checkForCheckedItems() 
{
    for (int i = 0; i < ui.treeWidget->topLevelItemCount(); ++i) {
        QTreeWidgetItem *item = ui.treeWidget->topLevelItem(i);
        if (item->checkState(0) == Qt::Checked) {
            return true;
        }
    }
    return false;
}

void ManageCapturesDialog::slotDeleteCurrent()
{
    QTreeWidgetItem *item = ui.treeWidget->currentItem();
    if (item == nullptr) {
        return;
    }
    removeItemFromTreeWidget(item);
    removeFile(item);
    delete item;
}

void ManageCapturesDialog::removeItemFromTreeWidget(QTreeWidgetItem *item) 
{
    const int index = ui.treeWidget->indexOfTopLevelItem(item);
    ui.treeWidget->takeTopLevelItem(index);
}

void ManageCapturesDialog::removeFile(QTreeWidgetItem *item) 
{
    QString filePath = item->data(0, Qt::UserRole).toString();
    if (!QFile::remove(filePath)) {
        qCDebug(KDENLIVE_LOG) << "Error removing file " << filePath;
    }
}

void ManageCapturesDialog::slotToggle()
{
    int count = ui.treeWidget->topLevelItemCount();
    for (int i = 0; i < count; ++i) {
        QTreeWidgetItem *item = ui.treeWidget->topLevelItem(i);
        if (item != nullptr) {
            toggleCheckStateForItem(item);
        }
    }
}

void ManageCapturesDialog::toggleCheckStateForItem(QTreeWidgetItem *item) 
{
    item->setCheckState(0, item->checkState(0) == Qt::Checked ? Qt::Unchecked : Qt::Checked);
}

QList<QUrl> ManageCapturesDialog::importFiles() const
{
    QList<QUrl> result;
    const int count = ui.treeWidget->topLevelItemCount();
    for (int i = 0; i < count; ++i) {
        QTreeWidgetItem *item = ui.treeWidget->topLevelItem(i);
        if (item != nullptr && isItemSelected(item)) {
            result.append(QUrl(item->data(0, Qt::UserRole).toString()));
        }
    }
    return result;
}

bool ManageCapturesDialog::isItemSelected(QTreeWidgetItem *item) const 
{
    return item->checkState(0) == Qt::Checked;
}
