/*
    SPDX-FileCopyrightText: 2008 Jean-Baptiste Mardelle <jb@kdenlive.org>

SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include <QPushButton>

#include <QUrl>

#include "ui_managecaptures_ui.h"

class ManageCapturesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ManageCapturesDialog(const QList<QUrl> &files, QWidget *parent = nullptr);
    ~ManageCapturesDialog() override;
    QList<QUrl> importFiles() const;
    bool checkForCheckedItems();
    bool isItemSelected(QTreeWidgetItem *item) const;
    void removeItemFromTreeWidget(QTreeWidgetItem *item);
    void removeFile(QTreeWidgetItem *item);
    void toggleCheckStateForItem(QTreeWidgetItem *item);

private slots:
    void slotRefreshButtons();
    void slotDeleteCurrent();
    void slotToggle();
    void slotCheckItemIcon();

private:
    Ui::ManageCaptures_UI ui{};
    QPushButton *importButton;
};
